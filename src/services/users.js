import jwt from 'jsonwebtoken'
import bcrypt from 'bcrypt'
import { User } from '../models'
import config from '../../config'
import { customValidators } from '../helpers'

export const authenticate = async ({ username, email, password }) => {
  const creds = username ? { username } : { email }

  const user = await User.findOne(creds)

  if(user && bcrypt.compareSync(password, user.hash)){
    const { hash, ...userWithoutHash } = user.toObject()
    const token = jwt.sign({ sub: user.id }, config.secret, { expiresIn: '7d' })

    return { ...userWithoutHash, token }
  }
}

export const create = async userParam => {
  if(await User.findOne({ username: userParam.username })){
    throw new Error(`Username (${userParam.username}) is already taken`)
  }

  if(await User.findOne({ email: userParam.email })){
    throw new Error(`Email (${userParam.email}) already exists on our database`)
  }

  if(customValidators.userPassword(userParam.password)){
    const user = new User(userParam)

    if(userParam.password){
      user.hash = bcrypt.hashSync(userParam.password, 10)
    }

    await user.save()
  }
  
}

export const update = async (id, userParam) => {
  const user = await User.findById(id);

  if(!user){
    throw new Error('User not found')
  }

  if(user.username !== userParam.username && await User.findOne({ username: userParam.username })) {
    throw new Error(`Username (${userParam.username}) is already taken`)
  }

  if(user.email !== userParam.email && await User.findOne({ email: userParam.email })) {
    throw new Error(`Email (${userParam.email}) is already on our database`)
  }

  if(customValidators.userPassword(userParam.password)){
    if(userParam.password){
      userParam.hash = bcrypt.hashSync(userParam.password, 10)
    }
  
    Object.assign(user, userParam)
  
    await user.save()
  }
  
}

export const _delete = async id => await User.findByIdAndDelete(id)

export const getAll = async () => await User.find().select('-hash')

export const getById = async id => await User.findById(id).select('-hash')

