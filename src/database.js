import mongoose from 'mongoose'
import { MONGOENV, MONGOPORT, MONGONAME } from './config'

const connection = `mongodb://${MONGOENV}:${MONGOPORT}/${MONGONAME}`

mongoose.connect(connection, { useCreateIndex: true, useNewUrlParser: true })
  .then(() => console.log( `MongoDB Connected at: ${connection}` ))
  .catch(err => console.log(err))

export default mongoose