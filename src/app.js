import express from 'express'
import bodyParser from 'body-parser'
import cors from 'cors'
import helmet from 'helmet'
import routes from './routes'
import { errorHandler, jwt } from './helpers'
import { FRONTEND_URL } from './config'

// Cors Options
const corsWhitelist = [ FRONTEND_URL ]
const corsOptions = (req, next) => {
  let options = { optionsSuccessStatus: 200 }
  const isListed = (corsWhitelist.indexOf(req.header('Origin')) !== -1)
  
  options = { origin: isListed, ...options }
  next(null, options)
}

// Middlewares
const app = express()

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(helmet())

app.use(cors(corsOptions))
app.use(jwt())

app.use('/api', routes )

app.use((req, res) => res.sendStatus(404))
app.use( errorHandler )

export default app
