export default [
  '/api/health-check',
  '/api/users/authenticate',
  '/api/users/register'
]