import expressJwt from 'express-jwt'
import validator from 'validator'
import config from '../../config.json'
import { userService } from '../services'
import authorizedRoutes from './authorized-routes'

export default jwt

function jwt(req, res, next) {
  const secret = config.secret
  return expressJwt({ secret, isRevoked }).unless({ path: authorizedRoutes })
}

async function isRevoked(req, payload, done) {
  if(!validator.isMongoId(payload.sub)){
    return done(null, true)
  }

  const user = await userService.getById(payload.sub)

  if(!user){
    return done(null, true)
  }

  done()
};