import validator from 'validator'

export const userPassword = pass => {
  if(!validator.isLength(pass, { min: 4 })){
    throw new Error( 'Password must be at least 4 characters minimum' )
  }

  if(!validator.isLength(pass, { max: 25 })){
    throw new Error( 'Password must be 25 characters maximum' )
  }

  return true
}

export const isMongoId = id => {
  if(!validator.isMongoId(id)){
    throw new Error( 'Need a valid id to proceed' )
  }

  return true
}