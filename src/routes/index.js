import express from 'express'
import users from './users'

const router = express.Router();
export default router

router.use('/health-check', (req, res) =>{
  res.status(200)
  res.end('ok')
})

router.use('/users', users )


