import express from 'express'
import { userService } from '../services'
import { customValidators } from '../helpers';

const router = express.Router();

router.post('/authenticate', async (req, res, next) => {
  try{
    const user = await userService.authenticate(req.body)
    if(user){
      res.json(user)
    }else{
      throw 'Wrong username/email or password'
    }
  }catch(e){ next(e) }
})

router.post('/register', async (req, res, next) => {
  try{
    await userService.create(req.body)
    res.json({ message: 'User created successfully' })
  }catch(e){ next(e) }
})

router.get('/', async (req, res, next) => {
  try{
    const users = await userService.getAll()
    if(users){
      res.json(users)
    }else{
      res.sendStatus(404)
    }
  }catch(e){ next(e) }
})

router.get('/current', async (req, res, next) => {
  try{
    if(customValidators.isMongoId(req.user.sub)){
      const user = await userService.getById(req.user.sub)
      if(user){
        res.json(user)
      }else{
        res.sendStatus(404)
      }
    }
  }catch(e){ next(e)}
})

router.get('/:id', async (req, res, next) => {
  try{
    if(customValidators.isMongoId(req.params.id)){
      const user = await userService.getById(req.params.id)
      if(user){
        res.json(user)
      }else{
        res.sendStatus(404)
      }
    }
  }catch(e){ next(e) }
})

router.put('/:id', async (req, res, next) => {
  try{
    if(customValidators.isMongoId(req.params.id)){
      await userService.update(req.params.id, req.body)
      res.json({ message: 'User updated successfully' })
    }
  }catch(e){ next(e) }
})

router.delete('/:id', async (req, res, next) => {
  try{
    if(customValidators.isMongoId(req.params.id)){
      await userService._delete(req.params.id)
      res.json({ message: 'User deleted successfully' })
    }
  }catch(e){ next(e) }
})

export default router

