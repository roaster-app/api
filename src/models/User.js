import mongoose from '../database'
import validator from 'validator';
const Schema = mongoose.Schema

const UserSchema = new Schema({
  username: { 
    type: String,
    trim: true,
    unique: true,
    required: true,
    maxlength: [ 20, 'Username is alowed to 20 characters maximum'],
    minlength: [ 3, 'Username must be at least 3 characters'],
  },

  hash: {
    type: String,
    required: true,
  },

  email: {
    type: String,
    trim: true,
    lowercase: true,
    unique: true,
    required: 'Email address is required',
    validate: [validator.isEmail, 'Please fill a valid email address'],
    match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, 'Please fill a valid email address']
  },

  name: {
    first: {
      type: String,
      required: true,
      maxlength: [ 20, 'Username is alowed to 20 characters maximum'],
    },
    last: {
      type: String,
      required: true,
      maxlength: [ 20, 'Username is alowed to 20 characters maximum'],
    },
  },

  created: {
    type: Date,
    default: Date.now
  }
});

UserSchema.virtual( 'fullName' ).get(function () {
  return this.name.first + ' ' + this.name.last;
});

UserSchema.set( 'toJSON', { virtuals: true });

export default mongoose.model( 'user', UserSchema )