import app from './app'
import { ENV, PORT, PROTOCOL, HOST } from './config'

const url = [ PROTOCOL, HOST, ':', PORT ].join( '' );

app.listen( PORT, console.log(`Server is listening on ${url} | Environment: ${ENV}`))