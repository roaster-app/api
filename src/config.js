const environment = process.env.NODE_ENV || 'dev'

const defaultPort = 9000
const defaultHost = 'localhost'
const defaultProtocol = 'http://'

const defaultMongoPort = '27017'
const defaultMongoName = 'roaster'

export const FRONTEND_URL = 'http://localhost:3000'

export const ENV = environment == 'dev' ? 'Development' : 'Production'
export const DEV = ( environment == 'dev' )
export const PROD = ( environment == 'prod' )

export const PORT = process.env.PORT || defaultPort
export const HOST = process.env.HOST || defaultHost
export const PROTOCOL = process.env.PROTOCOL || defaultProtocol

export const MONGOENV = environment == 'dev' ? defaultHost : 'mongo'
export const MONGOPORT = process.env.MONGO_PORT || defaultMongoPort
export const MONGONAME = process.env.MONGO_NAME || defaultMongoName
