FROM node:12.3.1-alpine

WORKDIR /usr/src/app

COPY package*.json ./

COPY config*.json ./

RUN apk add --no-cache --virtual .gyp python make g++ \
  && npm install --production \
  && apk del .gyp

COPY ./build ./build

EXPOSE 9000

CMD [ "npm", "run", "serve" ]